from django.contrib import admin
from emails.models import EmailTemplate
from django.contrib.sites.models import Site

admin.site.register(EmailTemplate)
admin.site.unregister(Site)

