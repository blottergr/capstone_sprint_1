"""
This file demonstrates writing tests using the unittest module. These will pass
when you run "manage.py test".

Replace this with more appropriate tests for your application.
"""

#from django.test import TestCase
#class TestEmailSettings(TestCase):
#		def sendEmail(self):
#			"""
#			Test that an outgoing email works
#			"""
from django.core import mail
from django.test import TestCase

class EmailTest(TestCase):
    def test_send_email(self):
        """
        Send test emails
        """
        #Send emails to files to be inspected
        # Send message.
        mail.send_mail('Subject here', 'Here is the test message.',
            'from@example.com', ['wburningham+delete@gmail.com'],
            fail_silently=False)

        # Test that one message has been sent.
        self.assertEqual(len(mail.outbox), 1)

        # Verify that the subject of the first message is correct.
        self.assertEqual(mail.outbox[0].subject, 'Subject here')
#
#
#class SimpleTest(TestCase):
#    def test_basic_addition(self):
#        """
#        Tests that 1 + 1 always equals 2.
#        """
#        self.assertEqual(1 + 1, 2)
#
#from django.utils import unittest
#from emails.models import EmailTemplate
#
#class AnimalTestCase(unittest.TestCase):
#    def setUp(self):
#        self.lion = Animal.objects.create(name="lion", sound="roar")
#        self.cat = Animal.objects.create(name="cat", sound="meow")
#
#    def testSpeaking(self):
#        self.assertEqual(self.lion.speak(), 'The lion says "roar"')
#        self.assertEqual(self.cat.speak(), 'The cat says "meow"')
