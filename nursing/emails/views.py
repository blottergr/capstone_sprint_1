from django.http import HttpResponse

def sendTestEmail(request):
	from django.core.mail import send_mail, mail_admins
	# from settings import ADMINS
	# send_mail('Subject here', 'Here is the message.', 'from@example.com', [ADMINS], fail_silently=False)

	mail_admins('Test email for the admins', 'This is a test email.', fail_silently=False)

	return HttpResponse("An email was sent to the site admins")
