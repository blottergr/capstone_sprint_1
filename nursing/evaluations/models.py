from django.db import models
from school import *


class Template(models.Model):
	name = models.CharField(max_length=140)
	TYPE_CHOICES = (
		 ('L', 'Site/Location'),
		 ('S', 'Student'),
		 ('P', 'Preceptor'),
	)
	type = models.CharField(max_length=1, choices=TYPE_CHOICES)
	start_date = models.DateField()
	end_date = models.DateField()

	def __unicode__(self):
		return self.name

class Section(models.Model):
	template = models.ForeignKey(Template)	
	name = models.CharField(max_length=140)
	description = models.TextField()

	def __unicode__(self):
		return self.name

class Question(models.Model):
	#section = models.ForeignKey(Section)	#this doesn't allow for questions to be reused
	sections = models.ManyToManyField(Section)
	name = models.CharField(max_length=140)
	description = models.CharField(max_length=140)
	options = models.ManyToManyField(Option)

	def __unicode__(self):
		return self.name

class Option(models.Model):
	name = models.CharField(max_length=140)
	value = models.CharField(max_length=140)

	def __unicode__(self):
		return self.name + "(" + self.value + ")"

class PlanOfActionOption(models.Model):
	question = models.ForeignKey(Question)	
	name = models.CharField(max_length=140)
	value = models.CharField(max_length=140)
	options = models.ManyToManyField(Option)

	def __unicode__(self):
		return self.name

class LearningOutcome(models.Model):
	questions = models.ManyToManyField(Question)
	name = models.CharField(max_length=140)
	type = models.CharField(max_length=140)
	
	def __unicode__(self):
		return self.name

class Evaluation(models.Model):
	template = models.ForeignKey(Template)	
	date = models.DateField()
	completed_by = models.ForeignKey(User)	

class StudentEvaluation(Evaluation):
	placement = models.ForeignKey(Placement)	#this has the student, preceptor, and site id's
	
	def get_absolute_url(self):
		return "/%i/" % self.id
	

class PreceptorEvaluation(Evaluation):
	preceptor = models.ForeignKey(Preceptor)	

	def get_absolute_url(self):
		return "/%i/" % self.id

class SiteEvaluation(Evaluation):
	site = models.ForeignKey(Site)	

	def get_absolute_url(self):
		return "/%i/" % self.id

class Answer(models.Model):
	evaluation = models.ForeignKey(Evaluation)	
	question = models.ForeignKey(Question)	
	value = models.TextField()

	def __unicode__(self):
		return self.value

class PlanOfAction(models.Model):
	evaluation = models.ForeignKey(StudentEvaluation)	
	poa = models.ForeignKey(PlanOfActionOption)	


#TODO
#set default now for evaluation timestamp
