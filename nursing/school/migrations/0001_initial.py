# encoding: utf-8
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models

class Migration(SchemaMigration):
    
    def forwards(self, orm):
        
        # Adding model 'Site'
        db.create_table('school_site', (
            ('city', self.gf('django.db.models.fields.CharField')(max_length=100, null=True, blank=True)),
            ('fax', self.gf('django.db.models.fields.CharField')(max_length=25, null=True, blank=True)),
            ('office_manager_phone', self.gf('django.db.models.fields.CharField')(max_length=25, null=True, blank=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=140)),
            ('office_manager_email', self.gf('django.db.models.fields.CharField')(max_length=25, null=True, blank=True)),
            ('office_manager', self.gf('django.db.models.fields.CharField')(max_length=14, null=True, blank=True)),
            ('email', self.gf('django.db.models.fields.EmailField')(max_length=75, null=True, blank=True)),
            ('phone', self.gf('django.db.models.fields.CharField')(max_length=25, null=True, blank=True)),
            ('street', self.gf('django.db.models.fields.CharField')(max_length=200, null=True, blank=True)),
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('zip_code', self.gf('django.db.models.fields.CharField')(max_length=10, null=True, blank=True)),
        ))
        db.send_create_signal('school', ['Site'])

        # Adding model 'SiteContract'
        db.create_table('school_sitecontract', (
            ('expiration_date', self.gf('django.db.models.fields.DateField')()),
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('contract_file', self.gf('django.db.models.fields.files.FileField')(max_length=100)),
            ('site', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['school.Site'])),
        ))
        db.send_create_signal('school', ['SiteContract'])

        # Adding model 'Placement'
        db.create_table('school_placement', (
            ('site', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['school.Site'])),
            ('term', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['school.Term'])),
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('student', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['people.Student'])),
            ('preceptor', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['people.Preceptor'])),
        ))
        db.send_create_signal('school', ['Placement'])

        # Adding model 'Term'
        db.create_table('school_term', (
            ('term_code', self.gf('django.db.models.fields.IntegerField')()),
            ('is_current', self.gf('django.db.models.fields.BooleanField')(default=False, blank=True)),
            ('id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('term_name', self.gf('django.db.models.fields.CharField')(max_length=20)),
        ))
        db.send_create_signal('school', ['Term'])
    
    
    def backwards(self, orm):
        
        # Deleting model 'Site'
        db.delete_table('school_site')

        # Deleting model 'SiteContract'
        db.delete_table('school_sitecontract')

        # Deleting model 'Placement'
        db.delete_table('school_placement')

        # Deleting model 'Term'
        db.delete_table('school_term')
    
    
    models = {
        'auth.group': {
            'Meta': {'object_name': 'Group'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '80'}),
            'permissions': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['auth.Permission']", 'symmetrical': 'False', 'blank': 'True'})
        },
        'auth.permission': {
            'Meta': {'unique_together': "(('content_type', 'codename'),)", 'object_name': 'Permission'},
            'codename': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'content_type': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['contenttypes.ContentType']"}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        'auth.user': {
            'Meta': {'object_name': 'User'},
            'date_joined': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '75', 'blank': 'True'}),
            'first_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'groups': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['auth.Group']", 'symmetrical': 'False', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_active': ('django.db.models.fields.BooleanField', [], {'default': 'True', 'blank': 'True'}),
            'is_staff': ('django.db.models.fields.BooleanField', [], {'default': 'False', 'blank': 'True'}),
            'is_superuser': ('django.db.models.fields.BooleanField', [], {'default': 'False', 'blank': 'True'}),
            'last_login': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'last_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'password': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'user_permissions': ('django.db.models.fields.related.ManyToManyField', [], {'to': "orm['auth.Permission']", 'symmetrical': 'False', 'blank': 'True'}),
            'username': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '30'})
        },
        'contenttypes.contenttype': {
            'Meta': {'unique_together': "(('app_label', 'model'),)", 'object_name': 'ContentType', 'db_table': "'django_content_type'"},
            'app_label': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'model': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        'people.preceptor': {
            'Meta': {'object_name': 'Preceptor', '_ormbases': ['auth.User']},
            'site': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['school.Site']"}),
            'user_ptr': ('django.db.models.fields.related.OneToOneField', [], {'to': "orm['auth.User']", 'unique': 'True', 'primary_key': 'True'})
        },
        'people.student': {
            'Meta': {'object_name': 'Student', '_ormbases': ['auth.User']},
            'byu_num': ('django.db.models.fields.CharField', [], {'max_length': '9', 'blank': 'True'}),
            'hair': ('django.db.models.fields.CharField', [], {'max_length': '11', 'blank': 'True'}),
            'hair_color': ('django.db.models.fields.CharField', [], {'max_length': '11'}),
            'person_num': ('django.db.models.fields.CharField', [], {'max_length': '11', 'blank': 'True'}),
            'user_ptr': ('django.db.models.fields.related.OneToOneField', [], {'to': "orm['auth.User']", 'unique': 'True', 'primary_key': 'True'})
        },
        'school.placement': {
            'Meta': {'object_name': 'Placement'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'preceptor': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['people.Preceptor']"}),
            'site': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['school.Site']"}),
            'student': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['people.Student']"}),
            'term': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['school.Term']"})
        },
        'school.site': {
            'Meta': {'object_name': 'Site'},
            'city': ('django.db.models.fields.CharField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '75', 'null': 'True', 'blank': 'True'}),
            'fax': ('django.db.models.fields.CharField', [], {'max_length': '25', 'null': 'True', 'blank': 'True'}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '140'}),
            'office_manager': ('django.db.models.fields.CharField', [], {'max_length': '14', 'null': 'True', 'blank': 'True'}),
            'office_manager_email': ('django.db.models.fields.CharField', [], {'max_length': '25', 'null': 'True', 'blank': 'True'}),
            'office_manager_phone': ('django.db.models.fields.CharField', [], {'max_length': '25', 'null': 'True', 'blank': 'True'}),
            'phone': ('django.db.models.fields.CharField', [], {'max_length': '25', 'null': 'True', 'blank': 'True'}),
            'street': ('django.db.models.fields.CharField', [], {'max_length': '200', 'null': 'True', 'blank': 'True'}),
            'zip_code': ('django.db.models.fields.CharField', [], {'max_length': '10', 'null': 'True', 'blank': 'True'})
        },
        'school.sitecontract': {
            'Meta': {'object_name': 'SiteContract'},
            'contract_file': ('django.db.models.fields.files.FileField', [], {'max_length': '100'}),
            'expiration_date': ('django.db.models.fields.DateField', [], {}),
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'site': ('django.db.models.fields.related.ForeignKey', [], {'to': "orm['school.Site']"})
        },
        'school.term': {
            'Meta': {'object_name': 'Term'},
            'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_current': ('django.db.models.fields.BooleanField', [], {'default': 'False', 'blank': 'True'}),
            'term_code': ('django.db.models.fields.IntegerField', [], {}),
            'term_name': ('django.db.models.fields.CharField', [], {'max_length': '20'})
        }
    }
    
    complete_apps = ['school']
