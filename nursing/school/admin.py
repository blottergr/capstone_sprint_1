from school.models import *
from emails.models import *
from django.contrib import admin

from logging import getLogger

# Get an instance of a logger
logger = getLogger('CAS')

def get_term(object):
    current_term = object.term[4:]
    if current_term == '1':
        return 'Fall'
    elif current_term == '3':
        return 'Spring'
    elif current_term == '4':
        return 'Summer'
    elif current_term == '5':
        return 'Winter'

get_term.short_description="Term"

def get_year(object):
    return object.term[:4]

get_year.short_description="Year"

class EmailHistoryInline(admin.TabularInline):
	model = EmailHistory
	extra = 0
	readonly_fields = ('recipient_email', 'email_message', 'email_sent',)


class PlacementAdmin(admin.ModelAdmin):
	list_filter = ['term']
	search_fields = ['student', 'site', 'preceptor', 'term']
	list_display = ['student', 'site', 'preceptor', 'term']
	list_display_links = ['student', 'site', 'preceptor', 'term']
	

	raw_id_fields = ('student','site','preceptor')
	# define the autocomplete_lookup_fields
	autocomplete_lookup_fields = {
		'fk': ['student', 'preceptor','site'],

	}
	inlines = [
		EmailHistoryInline,
	]
	def response_change(self, request, queryset):
		result = super(PlacementAdmin, self).response_change(request, queryset)
 		#import pdb; pdb.set_trace()
		queryset.email_placement_details()
 		return result



class SiteContractAdminInline(admin.TabularInline):
    model = SiteContract
    extra = 1


class SiteAdmin(admin.ModelAdmin):
    inlines = (SiteContractAdminInline,)




admin.site.register(Site, SiteAdmin)

admin.site.register(Placement, PlacementAdmin)
#admin.site.register(SiteContract)

